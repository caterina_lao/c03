import {Fragment, useState} from 'react';
import {Table, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import Swal from 'sweetalert2';

import AdminAllProducts from './AdminAllProducts';

export default function ArchivedProduct({archivedProdProp}){

	const {name, description, price, _id, img, inStock} = archivedProdProp;

	const [productId, setProductId] = useState('');

	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState(0);
	const [inStock, setInStock] = useState('');
	const [isActive, setIsActive] = useState('');

	const status = (productId) => {
		fetch(`http://localhost:4000/products/:productId/archive`, {
			method: 'PUT',
			headers: {
				'Content-Type' : 'application/json',
				'Authorization': `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				productId: productId
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data === true) {
				Swal.fire({
					title: 'Status Changed',
					icon: 'success',
					text: 'You have successfully changed the status of the Product'
				})
			}
		})
	}

	useEffect (() => {
		fetch(`http://localhost:4000/products/${productId}`)
		.then(res => res.json())
		.then(data => {

			setProductId(data.productId)
			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
			setInStock(data.inStock)
			setIsActive(data.isActive)
		})
		
	}, [productId])
	
	return(
		<Fragment>
			<Table striped bordered hover>
				<thead>
					<tr>
						<th>Product Id</th>
						<th>Product Name</th>
						<th>Description</th>
						<th>Price</th>
						<th>Quantity</th>
						<th>Status</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>{productId}</td>
						<td>{name}</td>
						<td>{description}</td>
						<td>{price}</td>
						<td>{inStock}</td>
						<td>
							{product.isActive !=== true ? 
							<Button variant = "primary" onClick = {() => status(productId)}>Set me as Active</Button>
							:
							<Link variant = "secondary" onClick = {() => status(productId)}>Set me as inactive</Link>
							}
					</td>
					</tr>
				</tbody>
			</Table>
		</Fragment>	
	)
}



